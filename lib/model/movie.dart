import 'dart:convert';
import 'package:flutter/services.dart';

class Movie {
  Movie({
    required this.title,
    required this.year,
    required this.rated,
    required this.released,
    required this.runtime,
    required this.genre,
    required this.director,
    required this.writer,
    required this.actors,
    required this.plot,
    required this.language,
    required this.country,
    required this.awards,
    required this.poster,
    required this.metascore,
    required this.imdbRating,
    required this.imdbVotes,
    required this.imdbID,
    required this.type,
    required this.response,
    required this.images,
  });
  late final String title;
  late final String year;
  late final String rated;
  late final String released;
  late final String runtime;
  late final String genre;
  late final String director;
  late final String writer;
  late final String actors;
  late final String plot;
  late final String language;
  late final String country;
  late final String awards;
  late String poster;
  late final String metascore;
  late final String imdbRating;
  late final String imdbVotes;
  late final String imdbID;
  late final String type;
  late final String response;
  late List<String> images;

  Movie.fromJson(Map<String, dynamic> json){
    title = json['Title'];
    year = json['Year'];
    rated = json['Rated'];
    released = json['Released'];
    runtime = json['Runtime'];
    genre = json['Genre'];
    director = json['Director'];
    writer = json['Writer'];
    actors = json['Actors'];
    plot = json['Plot'];
    language = json['Language'];
    country = json['Country'];
    awards = json['Awards'];
    poster = json['Poster'];
    metascore = json['Metascore'];
    imdbRating = json['imdbRating'];
    imdbVotes = json['imdbVotes'];
    imdbID = json['imdbID'];
    type = json['Type'];
    response = json['Response'];
    images = List.castFrom<dynamic, String>(json['Images']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['Title'] = title;
    _data['Year'] = year;
    _data['Rated'] = rated;
    _data['Released'] = released;
    _data['Runtime'] = runtime;
    _data['Genre'] = genre;
    _data['Director'] = director;
    _data['Writer'] = writer;
    _data['Actors'] = actors;
    _data['Plot'] = plot;
    _data['Language'] = language;
    _data['Country'] = country;
    _data['Awards'] = awards;
    _data['Poster'] = poster;
    _data['Metascore'] = metascore;
    _data['imdbRating'] = imdbRating;
    _data['imdbVotes'] = imdbVotes;
    _data['imdbID'] = imdbID;
    _data['Type'] = type;
    _data['Response'] = response;
    _data['Images'] = images;
    return _data;
  }
  static Future<String> _loadMoviesAsset() async{
    return await rootBundle.loadString('assets/film.json');
  }

  static Future<List<Movie>> loadMovies() async{
    String jsonString = await _loadMoviesAsset();
    List<dynamic> jsonResponse = jsonDecode(jsonString)['movies'] as List;
    List<Movie> rep=[];
    jsonResponse.forEach((element) {rep.add(Movie.fromJson(element));});
    return rep;
  }

  String toString(){
    return title;
  }

  static getFilmBidon(){
    return Movie(title:"Film Bidon", year:"1234", rated: "rated", released:"2020", runtime:"123", genre:"bidon", director:"The director", writer:"The writer",
        actors:"Actors", plot:"Un super film bidon", language:"FR", country:"None",
        awards:"coucou", poster:"https://images-na.ssl-images-amazon.com/images/M/MV5BNzM2MDk3MTcyMV5BMl5BanBnXkFtZTcwNjg0MTUzNA@@._V1_SX1777_CR0,0,1777,999_AL_.jpg",
        metascore: "456", imdbRating: "800", imdbVotes:"123", imdbID: "1234", type:"movie",response:"True" , images:["https://images-na.ssl-images-amazon.com/images/M/MV5BMjEyOTYyMzUxNl5BMl5BanBnXkFtZTcwNTg0MTUzNA@@._V1_SX1500_CR0,0,1500,999_AL_.jpg",
          "https://images-na.ssl-images-amazon.com/images/M/MV5BNzM2MDk3MTcyMV5BMl5BanBnXkFtZTcwNjg0MTUzNA@@._V1_SX1777_CR0,0,1777,999_AL_.jpg",
          "https://images-na.ssl-images-amazon.com/images/M/MV5BMTY2ODQ3NjMyMl5BMl5BanBnXkFtZTcwODg0MTUzNA@@._V1_SX1777_CR0,0,1777,999_AL_.jpg",
          "https://images-na.ssl-images-amazon.com/images/M/MV5BMTMxOTEwNDcxN15BMl5BanBnXkFtZTcwOTg0MTUzNA@@._V1_SX1777_CR0,0,1777,999_AL_.jpg",
          "https://images-na.ssl-images-amazon.com/images/M/MV5BMTYxMDg1Nzk1MV5BMl5BanBnXkFtZTcwMDk0MTUzNA@@._V1_SX1500_CR0,0,1500,999_AL_.jpg"]);
  }

}