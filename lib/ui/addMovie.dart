import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '/model/movie.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:films/ui/home.dart';
import 'package:intl/intl.dart' ;


class AddMovie extends StatelessWidget{
  /*
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title: Text("Add a movie"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: (){
            Navigator.pop(context,Movie.getFilmBidon());
          },
          child: Text("Ajouter un film bidon"),
        ),
      ),
    );
  }
  */

  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
        title: const Text("Go back"),
    backgroundColor: Colors.blueGrey.shade900,

    ),
    backgroundColor: Colors.blueGrey.shade900,
    body:




      FormBuilder(
      key: _formKey,
      child:
        Center( child:
          Card( child :
              Container(
                margin: const EdgeInsets.all(10.0),
                padding: const EdgeInsets.all(10.0),
                width: 900,
                child:
                ListView( children: [
                  FormBuilderTextField(name: 'title', validator: FormBuilderValidators.required(), decoration: const InputDecoration(labelText: "*Titre :")),
                  FormBuilderTextField(
                      name: 'year',
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(),
                        FormBuilderValidators.integer(),
                      ]),
                      decoration: const InputDecoration(labelText: "*Année de réalisation :")),
                  FormBuilderDateTimePicker(
                      name: 'released',
                      inputType: InputType.date,
                      validator: FormBuilderValidators.required(),
                      decoration: const InputDecoration(labelText: "*Date de sortie :")),
                  FormBuilderTextField(
                      name: 'runtime',
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(),
                        FormBuilderValidators.integer(),
                      ]),
                      decoration: const InputDecoration(labelText: "*Durée (en minutes) :")),
                  FormBuilderCheckboxGroup (
                      name: 'genre',
                      decoration: const InputDecoration(
                          labelText: "*Genre :"
                      ),
                      options: const [
                        FormBuilderFieldOption(value: "Action"),
                        FormBuilderFieldOption(value: "Adventure"),
                        FormBuilderFieldOption(value: "Sci-Fi"),
                        FormBuilderFieldOption(value: "Drama"),
                        FormBuilderFieldOption(value: "Horror"),
                        FormBuilderFieldOption(value: "Fantasy"),
                        FormBuilderFieldOption(value: "Thriller"),
                        FormBuilderFieldOption(value: "Biography"),
                        FormBuilderFieldOption(value: "Comedy"),
                        FormBuilderFieldOption(value: "Crime"),
                        FormBuilderFieldOption(value: "History"),
                      ],
                  ),
                  FormBuilderTextField(name: 'director', validator: FormBuilderValidators.required(), decoration: const InputDecoration(labelText: "*Réalisateur :")),
                  FormBuilderTextField(name: 'writer', validator: FormBuilderValidators.required(), decoration: const InputDecoration(labelText: "*Scénariste :")),
                  FormBuilderTextField(name: 'actors', validator: FormBuilderValidators.required(), decoration: const InputDecoration(labelText: "*Acteurs :")),
                  FormBuilderTextField(name: 'plot', validator: FormBuilderValidators.required(), decoration: const InputDecoration(labelText: "*Synopsis :")),
                  FormBuilderTextField(name: 'language', validator: FormBuilderValidators.required(), decoration: const InputDecoration(labelText: "*Langue :")),
                  FormBuilderTextField(name: 'country', validator: FormBuilderValidators.required(), decoration: const InputDecoration(labelText: "*Pays :")),
                  FormBuilderTextField(name: 'awards', decoration: const InputDecoration(labelText: "Récompenses :")),
                  FormBuilderTextField(name: 'metascore', decoration: const InputDecoration(labelText: "Metascore :")),
                  FormBuilderRadioGroup(
                      name: 'type',
                      validator: FormBuilderValidators.required(),
                      decoration: const InputDecoration(
                          labelText: "*Type :"
                      ),
                      options: const [
                        FormBuilderFieldOption(value: "movie"),
                        FormBuilderFieldOption(value: "series"),
                        FormBuilderFieldOption(value: "autre"),
                      ],
                  ),
                  FormBuilderTextField(name: 'poster', validator: FormBuilderValidators.url(), decoration: const InputDecoration(labelText: "Affiche :")),
                  ElevatedButton(
                    onPressed: () {
                      _formKey.currentState?.save();
                      if (_formKey.currentState!.validate()) {
                        final formData = _formKey.currentState?.value;
                        // formData = { 'field1': ..., 'field2': ..., 'field3': ... }
                        // do something with the form data
                        _addNewMovie(context, formData);
                      }
                    },
                    child: const Text('Ajouter le film'),
                  ),
                  const Text("*Required", style: TextStyle(color: Colors.red)),
                ]),
              )
          )
        )
    )
    );
  }

  void _addNewMovie(BuildContext context, Map<String,dynamic>? data) {
    String poster = data?['poster'] ?? "https://assets.stickpng.com/thumbs/5a4613e5d099a2ad03f9c995.png" ;
    List<String> images = [poster] ;
    String year = data!['year'].toString();
    String runtime = "${data['runtime']} min.";
    String released = DateFormat('yyyy-MM-dd').format(data['released']);
    String genre = data['genre'].join(", ") ;
    String awards = data['awards'] ?? "N/A" ;
    String metascore = data['metascore'] ?? "N/A" ;
    Movie movie = Movie(title: data['title'], year: year, rated: "", released: released, runtime: runtime, genre: genre,
      director: data['director'], writer: data['writer'], actors: data['actors'], plot: data['plot'], language: data['language'], country: data['country'],
      awards: awards, poster: poster, metascore: metascore, imdbRating: "N/A", imdbVotes: "N/A", imdbID: "N/A", type: data['type'],
      response: "True", images: images ) ;

    Navigator.pop(context,movie); //Navigator permet de changer de route
  }
}
